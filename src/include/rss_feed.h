/*
 * rss_feed.h
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef rss_feed_h
#define rss_feed_h

#include <glib-object.h>

typedef struct _RssFeed RssFeed;
typedef struct _RssFeedClass   RssFeedClass;

#define RSS_FEED_TYPE (rss_feed_get_type())
#define RSS_FEED(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), RSS_FEED_TYPE, RssFeed))
#define IS_RSS_FEED(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RSS_FEED_TYPE))
#define RSS_FEED_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), RSS_FEED_TYPE, RssFeedClass))
#define IS_RSS_FEED_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), RSS_FEED_TYPE))
#define RSS_FEED_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), RSS_FEED_TYPE, RssFeedClass))

/* RssFeed object */
struct _RssFeed {
        GObject parent;

        /* Eventually making these private */
        gchar *title;
        gchar *link;
        gchar *description;
        gchar *copyright;

        /* Any items we may have */
        GList *items;
};

/* RssFeed class definition */
struct _RssFeedClass {
        GObjectClass parent_class;
};

/* Boilerplate GObject code */
static void rss_feed_class_init(RssFeedClass *klass);
static void rss_feed_init(RssFeed *reader);
static void rss_feed_dispose(GObject *object);
GType rss_feed_get_type(void);

/* RssFeed methods */
RssFeed* rss_feed_new(void);

#endif /* rss_feed_h */
