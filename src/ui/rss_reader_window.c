/*
 * rss_reader_window.c
 * 
 * Copyright 2013 Ikey Doherty <ikey@solusos.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include "config.h"
#include "../include/rss_reader_window.h"
#include "../include/rss_reader_panel.h"
#include <string.h>

G_DEFINE_TYPE_WITH_PRIVATE(RssReaderWindow, rss_reader_window, G_TYPE_OBJECT);

static void refresh_cb(GtkWidget *widget, gpointer userdata);
static void install_styles(RssReaderWindow *self);
static void set_row_style(GtkWidget *widget, gpointer userdata);
static void rss_change(GtkWidget *widget, gpointer userdata);
static void show_who_made_it(GtkWidget *widget, gpointer userdata);

static enum {
        RSS_COLUMN_LABEL,
        RSS_COLUMN_FEED,
        RSS_MAXCOLUMNS
} readercolumns;

/* Initialisation */
static void rss_reader_window_class_init(RssReaderWindowClass *klass)
{
        /* Nothing yet */
}

static void rss_reader_window_init(RssReaderWindow *self)
{
        GtkWidget *window;
        GtkWidget *header;
        GtkWidget *refresh_button, *refresh_image;
        GtkIconTheme *icon_theme;
        GdkPixbuf *refresh_pixbuf;
        GtkWidget *scroller, *listbox;
        GtkWidget *toolbar, *combo;
        GtkListStore *store;
        GtkToolItem *combo_item;
        GtkToolItem *about, *separator;
        GtkCellRenderer *cell_renderer;
        GtkWidget *layout;
        GtkStyleContext *toolbar_style;
        GtkTreeIter iter;

        self->priv = rss_reader_window_get_instance_private(self);

        /* Create our RSS parser */
        self->priv->reader = rss_reader_new();

        install_styles(self);

        /* Initialize our window */
        window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        self->priv->window = window;

        /* Set our window up */
        gtk_window_set_title(GTK_WINDOW(window), "Rss Reader");
        gtk_widget_set_size_request(window, 500, 600);
        gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
        gtk_window_set_icon_name(GTK_WINDOW(window), "internet-news-reader");
        gtk_window_set_wmclass(GTK_WINDOW(window), "RssReader", "Rss Reader");
        g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

        /* Create client side decorations */
        header = gtk_header_bar_new();
        self->priv->header = header;
        gtk_header_bar_set_title(GTK_HEADER_BAR(header), "Rss Reader");

        /* Grab the icontheme */
        icon_theme = gtk_icon_theme_get_default();
        self->priv->icon_theme = icon_theme;

        /* Get the refresh icon */
        refresh_pixbuf = gtk_icon_theme_load_icon(icon_theme, REFRESH_ICON,
                24, GTK_ICON_LOOKUP_GENERIC_FALLBACK, NULL);

        if (refresh_pixbuf) {
                refresh_button = gtk_button_new();
                refresh_image = gtk_image_new_from_pixbuf(refresh_pixbuf);
                gtk_button_set_image(GTK_BUTTON(refresh_button), refresh_image);
                self->priv->refresh_image = refresh_image;
                self->priv->refresh_pixbuf = refresh_pixbuf;
        } else {
                refresh_button = gtk_button_new_with_label("Refresh");
        }
        self->priv->refresh_button = refresh_button;
        gtk_button_set_relief(GTK_BUTTON(refresh_button), GTK_RELIEF_NONE);
        gtk_header_bar_pack_start(GTK_HEADER_BAR(header), refresh_button);
        g_signal_connect(refresh_button, "clicked", G_CALLBACK(refresh_cb), self);

        /* Always show the close button */
        gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(header), TRUE);
        gtk_window_set_titlebar(GTK_WINDOW(window), header);

        /* Main layout */
        layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
        gtk_container_add(GTK_CONTAINER(window), layout);

        /* Set up our toolbar */
        toolbar = gtk_toolbar_new();
        gtk_box_pack_start(GTK_BOX(layout), toolbar, FALSE, FALSE, 0);

        /* Primary toolbar for theme compatibility */
        toolbar_style = gtk_widget_get_style_context(toolbar);
        gtk_style_context_add_class(toolbar_style,
                GTK_STYLE_CLASS_PRIMARY_TOOLBAR);

        /* Create our model */
        store = gtk_list_store_new(RSS_MAXCOLUMNS, G_TYPE_STRING, G_TYPE_STRING);
        self->priv->store = store;

        /* Totally temporary.. */
        gtk_list_store_append(store, &iter);
        gtk_list_store_set(store, &iter,
                RSS_COLUMN_LABEL, "BBC News - World",
                RSS_COLUMN_FEED, "http://feeds.bbci.co.uk/news/world/rss.xml",
                -1);

        gtk_list_store_append(store, &iter);
        gtk_list_store_set(store, &iter,
                RSS_COLUMN_LABEL, "CNN Top Stories",
                RSS_COLUMN_FEED, "http://rss.cnn.com/rss/edition.rss",
                -1);

        /* And our combobox to enable choosing of feeds */
        combo = gtk_combo_box_new_with_model(GTK_TREE_MODEL(store));
        self->priv->combo = combo;
        g_signal_connect(combo, "changed", G_CALLBACK(rss_change), self);
        combo_item = gtk_tool_item_new();
        gtk_container_add(GTK_CONTAINER(combo_item), combo);
        gtk_container_add(GTK_CONTAINER(toolbar),
                GTK_WIDGET(combo_item));

        /* Create the cell renderer */
        cell_renderer = gtk_cell_renderer_text_new();
        gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), cell_renderer, TRUE);
        gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo), cell_renderer,
                "text", RSS_COLUMN_LABEL);

        /* Butt everything up to the end of the toolbar now */
        separator = gtk_separator_tool_item_new();
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(separator));
        gtk_separator_tool_item_set_draw(GTK_SEPARATOR_TOOL_ITEM(separator), FALSE);
        gtk_tool_item_set_expand(separator, TRUE);

        /* Because every app needs an ITookAgesMakinThisK button */
        about = gtk_tool_button_new(NULL, "About");
        gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(about),
                "help-about");
        g_signal_connect(about, "clicked", G_CALLBACK(show_who_made_it), (gpointer)self);
        gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(about));

        /* Set up our main content */
        scroller = gtk_scrolled_window_new(NULL, NULL);
        gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scroller),
                GTK_SHADOW_ETCHED_IN);
        listbox = gtk_list_box_new();
        self->priv->listbox = listbox;
        gtk_container_add(GTK_CONTAINER(scroller), listbox);

        gtk_box_pack_start(GTK_BOX(layout), scroller, TRUE, TRUE, 0);

        gtk_widget_show_all(window);

        /* This will force a refresh */
        gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);

}

static void rss_reader_window_dispose(GObject *object)
{
        RssReaderWindow *self;

        self = RSS_READER_WINDOW(object);

        /* Destroy our reader */
        g_object_unref(self->priv->reader);

        /* Destroy our window */
        gtk_window_destroy(self->priv->window);
        g_object_unref(self->priv->window);

        /* Kill our icontheme reference */
        g_object_unref(self->priv->icon_theme);

        if (self->priv->refresh_pixbuf)
                g_object_unref(self->priv->refresh_pixbuf);

        if (self->priv->current_uri)
                g_free(self->priv->current_uri);

        g_object_unref(self->priv->store);

        /* Destruct */
        G_OBJECT_CLASS (rss_reader_window_parent_class)->dispose (object);
}

/* Utility; return a new RssReaderWindow */
RssReaderWindow* rss_reader_window_new(void)
{
        RssReaderWindow *reader;

        reader = g_object_new(RSS_READER_WINDOW_TYPE, NULL);
        return RSS_READER_WINDOW(reader);
}

static void add_to_list(gpointer data, gpointer userdata)
{
        RssReaderWindow *self;
        RssItem *item;
        GtkWidget *panel;

        self = RSS_READER_WINDOW(userdata);
        item = RSS_ITEM(data);

        panel = GTK_WIDGET(rss_reader_panel_new(item));
        gtk_widget_show(panel);
        gtk_container_add(GTK_CONTAINER(self->priv->listbox), panel);
}

static void rss_change(GtkWidget *widget, gpointer userdata)
{
        GtkComboBox *combo;
        RssReaderWindow *self;
        GValue title = G_VALUE_INIT;
        GValue feed = G_VALUE_INIT;
        GtkTreeIter iter;
        const gchar *k_title, *k_feed;

        combo = GTK_COMBO_BOX(widget);
        self = RSS_READER_WINDOW(userdata);

        if (!gtk_combo_box_get_active_iter(combo, &iter))
                return;
        gtk_tree_model_get_value(GTK_TREE_MODEL(self->priv->store),
                &iter, RSS_COLUMN_LABEL, &title);
        gtk_tree_model_get_value(GTK_TREE_MODEL(self->priv->store),
                &iter, RSS_COLUMN_FEED, &feed);

        k_title = g_value_get_string(&title);
        k_feed = g_value_get_string(&feed);

        /* May need to change the title in the future */
        self->priv->current_uri = g_strdup(k_feed);

        /* Cause our view to refresh */
        refresh_cb(self->priv->refresh_button, self);
        g_value_unset(&title);
        g_value_unset(&feed);
}

static void refresh_cb(GtkWidget *widget, gpointer userdata)
{
        RssReaderWindow *self;
        RssFeed *feed;

        self = RSS_READER_WINDOW(userdata);

        gtk_widget_set_sensitive(widget, FALSE);
        if (!rss_reader_parse_uri(self->priv->reader,
                self->priv->current_uri))
                goto end;

        feed = self->priv->reader->feed;

        /* Cleanup any existing items */
        gtk_container_foreach(GTK_CONTAINER(self->priv->listbox),
                (GtkCallback)&gtk_widget_destroy, NULL);

        /* Add items from the feed to our view */
        g_list_foreach(feed->items, add_to_list, self);

        /* Set up some padding, etc. */
        gtk_container_foreach(GTK_CONTAINER(self->priv->listbox),
                (GtkCallback)&set_row_style, NULL);        

end:
        gtk_widget_set_sensitive(widget, TRUE);
}

/* Pop up an AboutDialog. Because people care who wrote this. */
static void show_who_made_it(GtkWidget *widget, gpointer userdata)
{
        RssReaderWindow *self;

        self = RSS_READER_WINDOW(userdata);

        const gchar* authors[] = {
                "Ikey Doherty <ikey@solusos.com>"
        };
        const gchar* comments = "Simplistic RSS Reader. No frills.";
        const gchar* copyright = "Copyright (C) Ikey Doherty 2013";
        gtk_show_about_dialog(GTK_WINDOW(self->priv->window),
                "authors", authors,
                "comments", comments,
                "copyright", copyright,
                "logo-icon-name", "internet-news-reader",
                "program-name", PACKAGE_NAME,
                "license-type", GTK_LICENSE_GPL_2_0,
                "version", PACKAGE_VERSION,
                "website", "http://www.solusos.com",
                NULL);
}

static void install_styles(RssReaderWindow *self)
{
        GtkCssProvider *css_provider;
        GdkScreen *screen;
        gchar *path;

        path = g_strdup_printf("%s/style.css", DATADIR);

        if (!path)
                return;
        css_provider = gtk_css_provider_new();
        gtk_css_provider_load_from_path(css_provider, path, NULL);
        screen = gdk_screen_get_default();
        gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(css_provider),
                GTK_STYLE_PROVIDER_PRIORITY_USER);

        g_free(path);
}

static void set_row_style(GtkWidget *widget, gpointer userdata)
{
        gtk_widget_set_margin_top(widget, 5);
        gtk_widget_set_margin_left(widget, 10);
        gtk_widget_set_margin_right(widget, 10);
        gtk_widget_set_margin_bottom(widget, 5);
        /*gtk_widget_set_name(widget, 'rss-row');*/
}
